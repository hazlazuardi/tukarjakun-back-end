package csui.advpro2021.groupassignment.controller;

import csui.advpro2021.groupassignment.entity.Jakun;
import csui.advpro2021.groupassignment.entity.User;
import csui.advpro2021.groupassignment.security.AuthenticationRequest;
import csui.advpro2021.groupassignment.security.AuthenticationResponse;
import csui.advpro2021.groupassignment.security.JwtUtil;
import csui.advpro2021.groupassignment.security.UserDetailsServiceImpl;
import csui.advpro2021.groupassignment.service.JakunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@CrossOrigin(origins = { "${url.local:http://localhost:3000}",
    "${url.frontend:https://tukarjakun.herokuapp.com}",
    "${url.backend:https://adpro-group-stage.herokuapp.com}"
    })
@RestController
@RequestMapping("/api")
public class MainController {

    @Autowired
    private JakunService jakunService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    private JwtUtil jwtUtil;

    @GetMapping("/all-jakun")
    public ResponseEntity retrieveAllJakun() {
        List<Object> jakunList = jakunService.getAllJakun();
        if (jakunList.size() != 0){
            return ResponseEntity.status(HttpStatus.OK).body(jakunList);
        } else {
            return new ResponseEntity("No Jakun available", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{ownerId}")
    public ResponseEntity getJakun(@PathVariable String ownerId) {
        Jakun toReturn = jakunService.getJakun(ownerId);
        if (toReturn != null) {
            return new ResponseEntity(toReturn, HttpStatus.OK);
        } else {
            return new ResponseEntity("Owner not found", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add-jakun", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity uploadJakun(@Valid @RequestBody Jakun jakun) {
            jakunService.addJakun(jakun);
            return ResponseEntity.ok(jakun);
    }

    @DeleteMapping(value = "/{ownerId}")
    public ResponseEntity deleteJakun(@PathVariable String ownerId){
        jakunService.deleteJakun(ownerId);
        return ResponseEntity.status(HttpStatus.OK)
            .body("Deleted");
    }

    @GetMapping(value = "/user")
    public ResponseEntity userPage() {
        return ResponseEntity.status(HttpStatus.OK).body("Hello User!");
    }

    @PostMapping(value = "/authenticate", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                    authenticationRequest.getUsername(),
                    authenticationRequest.getPassword())
            );
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect Username or Password", e);
        }
        final UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(authenticationRequest.getUsername());
        final String jwt = jwtUtil.generateToken(userDetails);
        return ResponseEntity.status(HttpStatus.OK).body(new AuthenticationResponse(jwt));
    }

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity registerNewUser(@Valid @RequestBody User user) {
        userDetailsServiceImpl.registerNewUser(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(user);
    }

    //    @GetMapping(value = "/basicauth")
//    public AuthenticationBean authenticate() {
//        //throw new RuntimeException("Some Error has Happened! Contact Support at ***-***");
//        return new AuthenticationBean("You are authenticated");
//    }

}
