package csui.advpro2021.groupassignment.service;

import csui.advpro2021.groupassignment.entity.Jakun;
import csui.advpro2021.groupassignment.repository.JakunRepository;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JakunService {
    @Autowired
    private JakunRepository jakunRepository;

    public Jakun addJakun(Jakun jakun) {
        jakunRepository.save(jakun);
        return jakun;
    }

    public List<Object> getAllJakun() {
        List<Object> jakunList = new ArrayList<>();
        jakunRepository.findAll().forEach(jakunList::add);
        return jakunList;
    }

    public Jakun getJakun(String id){
        Jakun toReturn = jakunRepository.findByOwnerId(id);
        return toReturn;
    }

    public void deleteJakun(String id) {
        Jakun toDelete = jakunRepository.findByOwnerId(id);
        jakunRepository.delete(toDelete);
    }
}
