package csui.advpro2021.groupassignment.entity;

public enum Size {
    S, M, L, XL, SPECIAL
}
