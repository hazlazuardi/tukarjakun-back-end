package csui.advpro2021.groupassignment.entity;

public enum Condition {
    MODIFIED, UNTOUCHED
}
