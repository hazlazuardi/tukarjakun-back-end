package csui.advpro2021.groupassignment.entity;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Jakun")
public class Jakun {

    @Id
    @NotBlank
    private String ownerId;

    @NotBlank
    private String description;

    @NotBlank
    private String contact;

    @NotNull(message = "Please provide faculty information")
    @Enumerated(EnumType.STRING)
    private Faculty faculty;

    @NotNull(message = "Please provide size information")
    private Size size;

    @NotNull(message = "Please provide condition information")
    @Enumerated(EnumType.STRING)
    private Condition condition;

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String owenerId) {
        this.ownerId = owenerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

}
