package csui.advpro2021.groupassignment.service;

import csui.advpro2021.groupassignment.entity.Condition;
import csui.advpro2021.groupassignment.entity.Faculty;
import csui.advpro2021.groupassignment.entity.Jakun;
import csui.advpro2021.groupassignment.entity.Size;
import csui.advpro2021.groupassignment.repository.JakunRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class JakunServiceTest {

    @Mock
    private JakunRepository jakunRepository;

    @InjectMocks
    private JakunService jakunService;

    public static Jakun jakun = new Jakun();
    public static Jakun jakun1 = new Jakun();

    @BeforeAll
    public static void setUp(){
        jakun.setOwnerId("yowlie");
        jakun.setContact("rsc10");
        jakun.setDescription("yuk tuker heheh bosen aja mau yang baru");
        jakun.setCondition(Condition.UNTOUCHED);
        jakun.setFaculty(Faculty.FIA);
        jakun.setSize(Size.SPECIAL);

        jakun1.setOwnerId("owo");
        jakun1.setContact("rsc11");
        jakun1.setDescription("uwuwuwuowow");
        jakun1.setCondition(Condition.MODIFIED);
        jakun1.setFaculty(Faculty.FT);
        jakun1.setSize(Size.S);
    }

    @Test
    void addJakunTest(){
        jakunService.addJakun(jakun);
        verify(jakunRepository, times(1)).save(jakun);
    }

    @Test
    void getAllJakunTest(){
        jakunService.addJakun(jakun);
        jakunService.addJakun(jakun1);
        jakunService.getAllJakun();
        verify(jakunRepository, times(1)).findAll();
    }

    @Test
    void getJakunTest(){
        jakunService.addJakun(jakun);
        jakunService.addJakun(jakun1);
        jakunService.getJakun("owo");
        verify(jakunRepository, times(1)).findByOwnerId("owo");
    }

//    @Test
//    void deleteJakunTest(){
//        jakunService.addJakun(jakun);
//        jakunService.addJakun(jakun1);
//        jakunService.deleteJakun("owo");
//        verify(jakunRepository, times(1)).delete(jakun);
//    }
}
