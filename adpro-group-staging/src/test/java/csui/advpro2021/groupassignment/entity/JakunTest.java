package csui.advpro2021.groupassignment.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JakunTest {

    @Test
    void testIfJakunHasCorrectAttributesValuesWhenCreated(){
        Jakun j = new Jakun();
        j.setCondition(Condition.UNTOUCHED);
        j.setSize(Size.L);
        j.setContact("JenkyTheMan");
        j.setOwnerId("jen.ky");
        j.setDescription("It's ugly");
        j.setFaculty(Faculty.FIA);

        assertEquals(j.getCondition(), Condition.UNTOUCHED);
        assertEquals(j.getContact(), "JenkyTheMan");
        assertEquals(j.getSize(), Size.L);
        assertEquals(j.getOwnerId(), "jen.ky");
        assertEquals(j.getDescription(), "It's ugly");
        assertEquals(j.getFaculty(), Faculty.FIA);

    }

}
