package csui.advpro2021.groupassignment.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.groupassignment.entity.*;
import csui.advpro2021.groupassignment.repository.JakunRepository;
import csui.advpro2021.groupassignment.repository.UserRepository;
import csui.advpro2021.groupassignment.security.JwtUtil;
import csui.advpro2021.groupassignment.security.UserDetailsServiceImpl;
import csui.advpro2021.groupassignment.service.JakunService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = MainController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS) // So BeforeAll runs Per Class, not Per Method.
class MainControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @MockBean
    private AuthenticationManager authenticationManager;

    @MockBean
    private JwtUtil jwtUtil;

    @MockBean
    private JakunRepository jakunRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private JakunService jakunService;

    @MockBean
    private Jakun jakun;

    @MockBean
    private User user;

    @BeforeEach
    void setup(){
//        mockMvc = MockMvcBuilders
//            .webAppContextSetup(context)
//            .apply(springSecurity())
//            .build();
        jakun = new Jakun();
        jakun.setOwnerId("wishnu.hazmi");
        jakun.setContact("rsc7");
        jakun.setDescription("jadiii tu guys jakun yang aku ambil ternyata kekecilan huhu gimanadong, tuker yuu punya aku masih bagus ko wlau ada yg blong kena setrika bibi aku ktiduran xixi");
        jakun.setCondition(Condition.UNTOUCHED);
        jakun.setFaculty(Faculty.FASILKOM);
        jakun.setSize(Size.S);
        jakunService.addJakun(jakun);
        user = new User();
        user.setUsername("new");
        user.setPassword("123");
        userDetailsServiceImpl.registerNewUser(user);
    }

    @Test
    @WithMockUser("haz")
    void testRetrieveAllJakunAndItsAvailableShouldHTTPStatusOk() throws Exception {
        when(jakunService.getAllJakun()).thenReturn(List.of(jakun));
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .get("/api/all-jakun")
            .accept(MediaType.APPLICATION_JSON);
        mockMvc
            .perform(requestBuilder)
            .andExpect(status().isOk());
    }


    @Test
    @WithMockUser("haz")
    void testRetrieveAllJakunAndItsNotAvailableShouldHTTPStatusNotFound() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .get("/api/all-jakun")
            .accept(MediaType.APPLICATION_JSON);
        mockMvc
            .perform(requestBuilder)
            .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser("haz")
    void testFindJakunByOwnerFoundShouldHTTPStatusOK() throws Exception {
        when(jakunService.getJakun(Mockito.anyString())).thenReturn(jakun);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .get("/api/wishnu.hazmi")
            .accept(MediaType.APPLICATION_JSON);

        mockMvc
            .perform(requestBuilder)
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    @WithMockUser("haz")
    void testFindJakunByOwnerNotFoundShouldHTTPStatusNotFound() throws Exception {
        when(jakunService.getJakun(Mockito.eq("wishnu.hazmi"))).thenReturn(jakun);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .get("/api/{ownerId}", "wishnu.halow")
            .accept(MediaType.APPLICATION_JSON);

        mockMvc
            .perform(requestBuilder)
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser("haz")
    void testUploadJakunWithCompleteFieldsShouldHTTPStatusOK() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders
        .post("/api/add-jakun")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content("    {\n" +
                "        \"ownerId\": \"wishnu.hazmi\",\n" +
                "        \"description\": \"jadiii tu guys jakun yang aku ambil ternyata kekecilan huhu gimanadong, tuker yuu punya aku masih bagus ko wlau ada yg blong kena setrika bibi aku ktiduran xixi\",\n" +
                "        \"contact\": \"rsc7\",\n" +
                "        \"faculty\": \"FASILKOM\",\n" +
                "        \"size\": \"S\",\n" +
                "        \"condition\": \"UNTOUCHED\"\n" +
                "    }");

        mockMvc
            .perform(requestBuilder)
            .andExpect(status().isOk());
    }

    @Test
    @WithMockUser("haz")
    void testUploadJakunWithIncompleteFieldsShouldHTTPStatusBadRequest() throws Exception {
        mockMvc.perform(
            post("/api/add-jakun")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("    {\n" +
                    "        \"ownerId\": \"wishnu.hazmi\",\n" +
                    "        \"description\": \"jadiii tu guys jakun yang aku ambil ternyata kekecilan huhu gimanadong, tuker yuu punya aku masih bagus ko wlau ada yg blong kena setrika bibi aku ktiduran xixi\",\n" +
                    "        \"contact\": \"rsc7\",\n" +
                    "        \"faculty\": \"FASILKOM\",\n" +
                    "        \"size\": \"S\",\n" +
                    "    }")
        )
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser("haz")
    void testUploadJakunWithWrongOptionFieldsShouldHTTPStatusBadRequest() throws Exception {
        mockMvc.perform(
            post("/api/add-jakun")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("    {\n" +
                    "        \"ownerId\": \"wishnu.hazmi\",\n" +
                    "        \"description\": \"jadiii tu guys jakun yang aku ambil ternyata kekecilan huhu gimanadong, tuker yuu punya aku masih bagus ko wlau ada yg blong kena setrika bibi aku ktiduran xixi\",\n" +
                    "        \"contact\": \"rsc7\",\n" +
                    "        \"faculty\": \"FASILKOM\",\n" +
                    "        \"size\": \"S\",\n" +
                    "        \"condition\": \"Baru Kok\"\n" +
                    "    }")
        )
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser("haz")
    void testDeleteJakunByOwnerIdSuccess() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .delete("/api/wishnu.halo");
        mockMvc
            .perform(requestBuilder)
            .andExpect(status().isOk());
    }


    @Test
    @WithMockUser("haz")
    void userPage() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .get("/api/user");
        mockMvc
            .perform(requestBuilder)
            .andExpect(status().isOk())
        .andExpect(content().string("Hello User!"));
    }

    @Test
    void createAuthenticationToken() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .post("/api/authenticate")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(user));

        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk());
    }

    @Test
    void testCreateAuthenticationTokenAndIncorrectDetails() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .post("/api/authenticate")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content("    {\n" +
                "        \"username\": \"wishnu.hazmi\",\n" +
                "    }");

        mockMvc.perform(requestBuilder)
            .andExpect(status().isBadRequest());
    }

    @Test
    void registerNewUser() throws Exception {
        User newUser = new User();
        newUser.setUsername("new");
        newUser.setPassword("123");
        newUser.setActive(true);
        newUser.setRoles("ADMIN");
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .post("/api/register")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(newUser));

        mockMvc.perform(requestBuilder)
            .andExpect(status().isForbidden());
    }

    @Test
    void testRegisterNewUserAndDetailsNotComplete() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .post("/api/register")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(user));

        mockMvc.perform(requestBuilder)
            .andExpect(status().isBadRequest());
    }


    @Test
    @WithMockUser(username = "new", password = "123")
    void addInterest() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .post("/api/wishnu.hazmi/interested")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content("{\"username\" : \"new\"}");

        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "new", password = "123")
    void retrieveUserProfile() throws Exception {
        when(userDetailsServiceImpl.loadCustomUserByUsername(Mockito.eq("new"))).thenReturn(user);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .get("/api/profile/{username}", "new");

        mockMvc.perform(requestBuilder)
            .andDo(print())
            .andExpect(status().isOk())
        .andExpect(content().string(objectMapper.writeValueAsString(user)))
        ;
    }

    @Test
    @WithMockUser(username = "new", password = "123")
    void testRetrieveUserProfileAndNotFound() throws Exception {
        when(userDetailsServiceImpl.loadCustomUserByUsername(Mockito.eq("new"))).thenReturn(user);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .get("/api/profile/{username}", "oldy");

        mockMvc.perform(requestBuilder)
            .andDo(print())
            .andExpect(status().isNotFound())
        ;
    }

    @Test
    @WithMockUser(username = "new", password = "123")
    void searchJakun() throws Exception{
        List<Jakun> matched = new ArrayList<>();
        matched.add(jakun);
        when(jakunService.findJakun(Mockito.eq("wishnu.hazmi"))).thenReturn(matched);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .get("/api/search/{words}", "wishnu.hazmi");

        mockMvc
            .perform(requestBuilder)
            .andDo(print())
            .andExpect(status().isOk())
        .andExpect(content().string(objectMapper.writeValueAsString(matched)));
    }

    @Test
    @WithMockUser(username = "new", password = "123")
    void testSearchJakunAndNotFound() throws Exception{
        List<Jakun> matched = new ArrayList<>();
        matched.add(jakun);
        when(jakunService.findJakun(Mockito.eq("wishnu.hazmi"))).thenReturn(matched);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .get("/api/search/{words}", "wishnu.halow");

        mockMvc
            .perform(requestBuilder)
            .andDo(print())
            .andExpect(status().isNotFound());
    }

}

