package csui.advpro2021.groupassignment.entity;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import csui.advpro2021.groupassignment.utility.CustomSetSerializer;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Jakun")
public class Jakun {

    @Id
    @NotBlank
    private String ownerId;

    @NotBlank
    private String description;

    @NotBlank
    private String contact;

    @NotNull(message = "Please provide faculty information")
    @Enumerated(EnumType.STRING)
    private Faculty faculty;

    @NotNull(message = "Please provide size information")
    @Enumerated(EnumType.STRING)
    private Size size;

    @NotNull(message = "Please provide condition information")
    @Enumerated(EnumType.STRING)
    private Condition condition;

    @ManyToMany
    @JsonSerialize(using = CustomSetSerializer.class)
    private Set<User> savedByUser = new HashSet<>();

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String owenerId) {
        this.ownerId = owenerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }


    public void addSavedByUser(User user) {
        this.savedByUser.add(user);
        user.getSavedJakun().add(this);
    }

    public void removeSavedByUser(User user) {
        this.savedByUser.remove(user);
        user.getSavedJakun().remove(this);
    }


}
