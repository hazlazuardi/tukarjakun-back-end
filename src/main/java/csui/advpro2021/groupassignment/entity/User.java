package csui.advpro2021.groupassignment.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "UserDetail")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    @NotBlank
    private String roles;

    private Boolean active;

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<Jakun> getSavedJakun() {
        return savedJakun;
    }

    public void setSavedJakun(Set<Jakun> savedJakun) {
        this.savedJakun = savedJakun;
    }

    @ManyToMany(mappedBy = "savedByUser")
    private Set<Jakun> savedJakun = new HashSet<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public Boolean getActive() {
        return active;
    }

    public void addSavedJakun(Jakun jakun) {
        savedJakun.add(jakun);
    }

    public void removeSavedJakun(Jakun jakun) { savedJakun.remove(jakun);}

//    public List<Jakun> getInterestedJakun() {
//        return interestedJakun;
//    }
//
//    public void addInterestedJakun(Jakun jakun) {
//        interestedJakun.add(jakun);
//    }

}
