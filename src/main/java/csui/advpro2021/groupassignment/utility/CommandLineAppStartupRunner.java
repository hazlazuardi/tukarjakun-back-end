//package csui.advpro2021.groupassignment.utility;
//
//import csui.advpro2021.groupassignment.entity.User;
//import csui.advpro2021.groupassignment.repository.UserRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.stereotype.Component;
//
//import java.util.Optional;
//
//@Component
//public class CommandLineAppStartupRunner implements CommandLineRunner {
//    @Autowired
//    UserRepository userRepository;
//
//    @Override
//    public void run(String...args) throws Exception {
//        Optional<User> user = userRepository.findByUsername("admin");
//        if (user.isPresent()) {
//            return;
//        } else {
//            User admin = new User();
//            admin.setUsername("admin");
//            admin.setPassword("admin");
//            admin.setRoles("ADMIN");
//            admin.setActive(true);
//            userRepository.save(admin);
//        }
//    }
//}
