package csui.advpro2021.groupassignment.utility;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import csui.advpro2021.groupassignment.entity.User;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class CustomSetSerializer extends StdSerializer<Set<User>> {

    public CustomSetSerializer() {
        this(null);
    }

    public CustomSetSerializer(Class<Set<User>> t) {
        super(t);
    }

    @Override
    public void serialize(Set<User> userList, JsonGenerator generator, SerializerProvider provider) throws IOException {
        Set<String> ids = new HashSet<>();
        for (User user : userList) {
            ids.add(user.getUsername());
        }
        generator.writeObject(ids);
    }
}
