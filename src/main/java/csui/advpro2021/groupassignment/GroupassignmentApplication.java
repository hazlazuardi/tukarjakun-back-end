package csui.advpro2021.groupassignment;

import csui.advpro2021.groupassignment.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
public class GroupassignmentApplication {

    @Value("${url.local:http://localhost:3000}")
    private String urlLocal;

    @Value("${url.frontend:https://tukarjakun.herokuapp.com}")
    private String urlFrontend;

    @Value("${url.backend:https://adpro-group-stage.herokuapp.com}")
    private String urlBackend;

	public static void main(String[] args) {
		SpringApplication.run(GroupassignmentApplication.class, args);
	}
}
