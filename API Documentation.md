## https://tukarjakun.herokuapp.com/
User can see list of all available jakun to be swapped
#### Expected input:
- None
#### Expected output:
- None
#### Possible error cases:
- None

## https://tukarjakun.herokuapp.com/register
User input their details to create an account
#### Expected input:
- Username, password, and role
#### Expected output:
- Redirect to homepage after registering
#### Possible error cases:
- Already existing username
- Password too short
- Empty field(s)

## https://tukarjakun.herokuapp.com/login
User input their credentials to login to TukarJakun
#### Expected input:
- Username and password
#### Expected output:
- Redirect to homepage after login
#### Possible error cases:
- Wrong username
- Wrong password
- Empty field(s)

## https://tukarjakun.herokuapp.com/search/{words}
User input their criterias for jakun they want to swap with
#### Expected input:
- Keywords for the jakuns that they want to find; could be faculty, size, condition, or owner id
#### Expected output:
- Redirect to list of matched jakuns
#### Possible error cases:
- Typos when inserting the keywords

## https://tukarjakun.herokuapp.com/add-jakun
User input their jakun details to be displayed
#### Expected input:
- Size
- Condition
#### Expected output:
- Redirect to list of jakuns
#### Possible error cases:
- Wrong Size
- Wrong Condition
- Empty field(s)

